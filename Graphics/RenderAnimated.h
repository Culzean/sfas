//
// Source files for Search For A Star competition
// Copyright (c) 2013 D.C. Waine
//
// Animated version of the basic sprite class
// 
// Add a summary of your changes here:
// 
// 

#pragma once
#include "d:\year3\sfas\rs2013-danielwaine\graphics\rendersprite.h"
#include "RenderSprite.h"
#include <vector>

namespace Engine
{

enum AnimationList {

			ke_walk,	//procceed in order clockwise
			ke_walkUp,
			ke_walkRight,
			ke_walkDown,
			ke_walkLeft,
			ke_attack0,
			ke_action0,
			ke_idle,
			ke_idleUp,
			ke_idleRight,
			ke_idleDown,
			ke_idleLeft,

			//A list all possible animation cycles
			ke_NumCycles
		};

struct Animation{
	int start, end,framesCnt;
	FLOAT fps;
	FLOAT totalTime;
	//default animation to begin
	Animation() : start(0),
					end(3),
					framesCnt(3-0),
					fps(0.8f)
	{totalTime = (FLOAT)(framesCnt * fps);}
};

struct AnimationCycle {
		//details of crt animation state submited to animation update
		INT	prevCycle;
		INT	crtCycle;
		FLOAT			crtAnimTime;
		INT				crtFrame;
		//default animation state
		AnimationCycle() : prevCycle(ke_walkUp),
							crtCycle(ke_walkRight),
							crtAnimTime(0),
							crtFrame(0)
		{}
	};

class RenderAnimated :
	public RenderSprite
{
public:
	RenderAnimated(LPDIRECT3DDEVICE9 p_dx_Device, HWND han_Window, float size, LPCWSTR textureStr);
	RenderAnimated(LPDIRECT3DDEVICE9 p_dx_Device, HWND han_Window, float size, LPCWSTR textureStr, D3DCOLOR color );
	virtual ~RenderAnimated(void);

	virtual void Init();
	virtual void Draw();
	virtual void Draw( const D3DMATRIX * pWorld );

	void Update( FLOAT dt, AnimationCycle &crtAnim );

private:

		D3DRECT					m_SrcRect;

		/*int				m_CrtCycle;
		Animation		m_CrtAnimation;
		int				m_CrtFrame;*/
		int				m_FrameTotalX; //total number of frammes for the sheet
		int				m_FrameTotalY;
		FLOAT			m_SheetTotalX; //sheet dimensions
		FLOAT			m_SheetTotalY;
		//FLOAT			m_AnimationTime;


		std::vector <Animation>		m_Animations;

		void UpdateBuffer();
		void FillVertices();
		void SetAnimation();

		VertexPosTex				cv_Vertices[4];
};

}
