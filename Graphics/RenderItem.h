//
// Source files for Search For A Star competition
// Copyright (c) 2013 L. Attwood
//
// RenderItem 2D square in 3D space for rendering game objects 
// 
// Add a summary of your changes here:
// 
// added vertex with texture coordinates
// moved vertex definitions into engine namespace to be more widely avaliable
// set public API method to be virtual
// moved member variables used for instatiation and rendering into protected
// refactored this to be a sprite 
//
#pragma once

#include <d3d9.h>
#include <d3dx9tex.h>
#include "RenderSprite.h"

namespace Engine
{

class RenderItem :
	public RenderSprite
{

public:
	RenderItem(LPDIRECT3DDEVICE9 p_dx_Device, HWND han_Window, float size, DWORD colour);
	virtual ~RenderItem(void);

	virtual void Init();
	virtual void Draw();
	virtual void Draw( const D3DMATRIX * pWorld );


private:

	VertexPosCol			cv_Vertices[4];
	void FillVertices();
	void FillIndices();

};
}