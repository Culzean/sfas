//
// Source files for Search For A Star competition
// Copyright (c) 2013 L. Attwood
//
// RenderItem 2D square in 3D space for rendering game objects 
// 
// Add a summary of your changes here:
// 
// added vertex with texture coordinates
// moved vertex definitions into engine namespace to be more widely avaliable
// set public API method to be virtual
// moved member variables used for instatiation and rendering into protected
// 

#pragma once
#include <d3d9.h>
#include <d3dx9tex.h>

namespace Engine
{

	struct VertexPosCol
	{
		FLOAT x, y, z;
		DWORD color;
	};

	struct VertexPosTex
	{
		FLOAT x, y, z;
		FLOAT s,t;
	}; 


class RenderSprite
{
public:
	RenderSprite(LPDIRECT3DDEVICE9 p_dx_device, HWND han_Window, float size, LPCWSTR textureStr);
	RenderSprite(LPDIRECT3DDEVICE9 p_dx_device, HWND han_Window, float size, LPCWSTR textureStr, D3DCOLOR color);
	virtual ~RenderSprite(void);

	virtual void Init();
	virtual void Draw();
	virtual void Draw( const D3DMATRIX * pWorld );

	virtual void Update( FLOAT dt ) {};

protected:
	D3DXMATRIX m_World;

	void LoadTexture( LPCWSTR texStr );

	float m_Size;
	HWND m_Window;
	LPDIRECT3DDEVICE9 m_pDxDevice;
	LPDIRECT3DVERTEXBUFFER9		p_dx_VertexBuffer;
	LPDIRECT3DINDEXBUFFER9		p_dx_IndexBuffer;

	float flt_Angle;
	DWORD m_Colour;
	short s_Indices[6];
	DWORD				m_VertexFormat;

	IDirect3DTexture9 *			p_dx_Texture;
	LPCWSTR						m_TextureStr;

	void FillIndices();

private:

	//some methods and variables not to be used by inheriting classes
	void FillVertices();

	VertexPosTex				cv_Vertices[4];
};

}
