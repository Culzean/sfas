#include "RenderAnimated.h"

using Engine::RenderAnimated;

RenderAnimated::RenderAnimated(LPDIRECT3DDEVICE9 p_dx_Device, HWND han_Window, float size, LPCWSTR textureStr, D3DCOLOR color ) : 
	RenderSprite(p_dx_Device, han_Window, size, textureStr,color)
{
	m_Animations = std::vector< Animation >(ke_NumCycles);
}

RenderAnimated::RenderAnimated(LPDIRECT3DDEVICE9 p_dx_Device, HWND han_Window, float size, LPCWSTR textureStr ) : 
								RenderSprite(p_dx_Device, han_Window, size, textureStr)
{
	m_Animations = std::vector< Animation >(ke_NumCycles);
}

RenderAnimated::~RenderAnimated() {
	m_Animations.clear();
}

void RenderAnimated::Init() 
{
	//load texture and find dimensions of sprite sheet
	LoadTexture( m_TextureStr );

	FillVertices();
	FillIndices();
	
	SetAnimation();

	m_VertexFormat = (D3DFVF_XYZ | D3DFVF_TEX1 );

	if (FAILED(m_pDxDevice->CreateVertexBuffer(4*sizeof(VertexPosTex), 0, m_VertexFormat , D3DPOOL_MANAGED, &p_dx_VertexBuffer, NULL)))
	{
		MessageBox(m_Window,L"Error while creating RenderItem VertexBuffer",L"FillVertices()",MB_OK);
	}

	VOID* p_Vertices;
	if (FAILED(p_dx_VertexBuffer->Lock(0, 4*sizeof(VertexPosTex), (void**)&p_Vertices, 0)))
	{
		MessageBox(m_Window,L"Error trying to lock RenderItem VertexBuffer",L"FillVertices()",MB_OK);
	}
	else
	{
		memcpy(p_Vertices, cv_Vertices, sizeof(cv_Vertices));
		p_dx_VertexBuffer->Unlock();
	}

	if (FAILED(m_pDxDevice->CreateIndexBuffer(6*sizeof(short), D3DUSAGE_WRITEONLY,D3DFMT_INDEX16,D3DPOOL_MANAGED,&p_dx_IndexBuffer,NULL)))
	{
		MessageBox(m_Window,L"Error while creating RenderItem IndexBuffer",L"FillIndices()",MB_OK);
	}

	VOID* p_Indices;
	if (FAILED(p_dx_IndexBuffer->Lock(0, 6*sizeof(short), (void**)&p_Indices, 0)))
	{
		MessageBox(m_Window,L"Error trying to lock RenderItem IndexBuffer",L"FillIndices()",MB_OK);
	}
	else
	{
		memcpy(p_Indices, s_Indices, sizeof(s_Indices));
		p_dx_IndexBuffer->Unlock();
	}
}

void RenderAnimated::Update( FLOAT dt, Engine::AnimationCycle &crtAnim )
{
	crtAnim.crtAnimTime += dt;

	if(crtAnim.crtAnimTime > ( m_Animations[crtAnim.crtCycle].totalTime * (crtAnim.crtFrame + 1 - m_Animations[crtAnim.crtCycle].start) / m_Animations[crtAnim.crtCycle].end  ) )
	{
		//next slide
		crtAnim.crtFrame++;

		if(crtAnim.crtFrame > m_Animations[crtAnim.crtCycle].end)
		{
			//reset cycle
			crtAnim.crtFrame = m_Animations[crtAnim.crtCycle].start;
			crtAnim.crtAnimTime = 0;
		}

		m_SrcRect.x1 = (LONG) (floor( crtAnim.crtFrame % m_FrameTotalX ) * m_SheetTotalX / m_FrameTotalX );
		m_SrcRect.y1 = (LONG) (floor( crtAnim.crtFrame / m_FrameTotalX ) * m_SheetTotalY / m_FrameTotalY );
		m_SrcRect.x2 = (LONG) (floor( crtAnim.crtFrame % m_FrameTotalX + 1 ) * m_SheetTotalX / m_FrameTotalX );
		m_SrcRect.y2 = (LONG) (floor( crtAnim.crtFrame / m_FrameTotalX + 1 ) * m_SheetTotalY / m_FrameTotalY );

		//then update the vertex information
		UpdateBuffer();
	}
}

void RenderAnimated::Draw()
{
	Draw(&m_World);
}

void RenderAnimated::Draw( const D3DMATRIX * pWorld )
{
	m_pDxDevice->SetTexture(0, p_dx_Texture);

	//how to draw texture


	m_pDxDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	m_pDxDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pDxDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	m_pDxDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	m_pDxDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_pDxDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pDxDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

	m_pDxDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	m_pDxDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	m_pDxDevice->SetTransform(D3DTS_WORLD, pWorld);
	m_pDxDevice->SetFVF( m_VertexFormat );
    m_pDxDevice->SetStreamSource(0, p_dx_VertexBuffer, 0, sizeof(VertexPosTex));
	m_pDxDevice->SetIndices(p_dx_IndexBuffer);
	m_pDxDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,0,0,4,0,2);

	m_pDxDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
}

void RenderAnimated::UpdateBuffer()
{
	//update the texture coordinates for each vertices
	cv_Vertices[0].s = m_SrcRect.x1 / m_SheetTotalX; //top left
	cv_Vertices[0].t = m_SrcRect.y1 / m_SheetTotalY;

	cv_Vertices[1].s = m_SrcRect.x2 / m_SheetTotalX; //top right
	cv_Vertices[1].t = m_SrcRect.y1 / m_SheetTotalY;

	cv_Vertices[2].s = m_SrcRect.x1 / m_SheetTotalX; //bottom left
	cv_Vertices[2].t = m_SrcRect.y2 / m_SheetTotalY;

	cv_Vertices[3].s = m_SrcRect.x2 / m_SheetTotalX; //bottom right
	cv_Vertices[3].t = m_SrcRect.y2 / m_SheetTotalY;

	//send new texture coordinates to buffer
	VOID* p_Vertices;
	if (FAILED(p_dx_VertexBuffer->Lock(0, 4*sizeof(VertexPosTex), (void**)&p_Vertices, 0)))
	{
		MessageBox(m_Window,L"Error trying to lock RenderItem VertexBuffer",L"FillVertices()",MB_OK);
	}
	else
	{
		memcpy(p_Vertices, cv_Vertices, sizeof(cv_Vertices));
		p_dx_VertexBuffer->Unlock();
	}
}

void RenderAnimated::FillVertices()
{
	const float half_size = m_Size * 0.5f;
     cv_Vertices[0].x = -half_size;
     cv_Vertices[0].y = half_size;
     cv_Vertices[0].z = 0.0f;
	 cv_Vertices[0].s = 0.0f;
	 cv_Vertices[0].t = 1.0f;
 
     cv_Vertices[1].x = half_size;
     cv_Vertices[1].y = half_size;
     cv_Vertices[1].z = 0.0f;
	 cv_Vertices[1].s = 1.0f;
	 cv_Vertices[1].t = 1.0f;
 
     cv_Vertices[2].x = -half_size;
     cv_Vertices[2].y = -half_size;
     cv_Vertices[2].z = 0.0f;
	 cv_Vertices[2].s = 0.0f;
	 cv_Vertices[2].t = 0.0f;

     cv_Vertices[3].x = half_size;
     cv_Vertices[3].y = -half_size;
     cv_Vertices[3].z = 0.0f;
	 cv_Vertices[3].s = 1.0f;
	 cv_Vertices[3].t = 0.0f;
}

void RenderAnimated::SetAnimation()
{
	D3DSURFACE_DESC desc;
	p_dx_Texture->GetLevelDesc(0,&desc);

	//this information will be provided with the tile sheet, if there is time
	m_FrameTotalX = 3;
	m_FrameTotalY = 4;

	m_SheetTotalX = (FLOAT)desc.Width;
	m_SheetTotalY = (FLOAT)desc.Height;

	m_Animations[ke_walkUp].start = 0;
	m_Animations[ke_walkUp].end = 2;
	m_Animations[ke_walkUp].framesCnt = m_Animations[ke_walkUp].end - m_Animations[ke_walkUp].start + 1;
	m_Animations[ke_walkUp].fps = 0.8f;
	m_Animations[ke_walkUp].totalTime = m_Animations[ke_walkUp].fps * m_Animations[ke_walkUp].framesCnt;

	m_Animations[ke_walkRight].start = 3;
	m_Animations[ke_walkRight].end = 5;
	m_Animations[ke_walkRight].framesCnt = m_Animations[ke_walkRight].end - m_Animations[ke_walkRight].start + 1;
	m_Animations[ke_walkRight].fps = 0.8f;
	m_Animations[ke_walkRight].totalTime = m_Animations[ke_walkRight].fps * m_Animations[ke_walkRight].framesCnt;

	m_Animations[ke_walkDown].start = 6;
	m_Animations[ke_walkDown].end = 8;
	m_Animations[ke_walkDown].framesCnt = m_Animations[ke_walkDown].end - m_Animations[ke_walkDown].start + 1;
	m_Animations[ke_walkDown].fps = 0.8f;
	m_Animations[ke_walkDown].totalTime = m_Animations[ke_walkDown].fps * m_Animations[ke_walkDown].framesCnt;

	m_Animations[ke_walkLeft].start = 9;
	m_Animations[ke_walkLeft].end = 11;
	m_Animations[ke_walkLeft].framesCnt = m_Animations[ke_walkLeft].end - m_Animations[ke_walkLeft].start + 1;
	m_Animations[ke_walkLeft].fps = 0.8f;
	m_Animations[ke_walkLeft].totalTime = m_Animations[ke_walkLeft].fps * m_Animations[ke_walkLeft].framesCnt;

	m_Animations[ke_idleUp].start = 0;
	m_Animations[ke_idleUp].end = 1;
	m_Animations[ke_idleUp].framesCnt = m_Animations[ke_idleUp].end - m_Animations[ke_idleUp].start + 1;
	m_Animations[ke_idleUp].fps = 4.1f;
	m_Animations[ke_idleUp].totalTime = m_Animations[ke_idleUp].fps * m_Animations[ke_idleUp].framesCnt;

	m_Animations[ke_idleRight].start = 3;
	m_Animations[ke_idleRight].end = 5;
	m_Animations[ke_idleRight].framesCnt = m_Animations[ke_idleRight].end - m_Animations[ke_idleRight].start + 1;
	m_Animations[ke_idleRight].fps = 4.1f;
	m_Animations[ke_idleRight].totalTime = m_Animations[ke_idleRight].fps * m_Animations[ke_idleRight].framesCnt;

	m_Animations[ke_idleDown].start = 6;
	m_Animations[ke_idleDown].end = 8;
	m_Animations[ke_idleDown].framesCnt = m_Animations[ke_idleDown].end - m_Animations[ke_idleDown].start + 1;
	m_Animations[ke_idleDown].fps = 4.1f;
	m_Animations[ke_idleDown].totalTime = m_Animations[ke_idleDown].fps * m_Animations[ke_idleDown].framesCnt;

	m_Animations[ke_idleLeft].start = 9;
	m_Animations[ke_idleLeft].end = 11;
	m_Animations[ke_idleLeft].framesCnt = m_Animations[ke_idleLeft].end - m_Animations[ke_idleLeft].start + 1;
	m_Animations[ke_idleLeft].fps = 4.1f;
	m_Animations[ke_idleLeft].totalTime = m_Animations[ke_idleLeft].fps * m_Animations[ke_idleLeft].framesCnt;
}