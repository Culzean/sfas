#include "RenderSprite.h"

using Engine::RenderSprite;

RenderSprite::RenderSprite(LPDIRECT3DDEVICE9 p_dx_device, HWND han_Window, float size, LPCWSTR textureStr) : 
	m_Size(size), m_Window(han_Window), m_pDxDevice(p_dx_device), p_dx_VertexBuffer(0), p_dx_IndexBuffer(0), flt_Angle(0), m_Colour( D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f) ), m_TextureStr(textureStr), m_VertexFormat(D3DFVF_XYZ | D3DFVF_TEX1 )				
{
	D3DXMatrixIdentity( &m_World );
	p_dx_Texture =nullptr;
	//set format to a basic default. But mean to reset this on init
}

RenderSprite::RenderSprite(LPDIRECT3DDEVICE9 p_dx_device, HWND han_Window, float size, LPCWSTR textureStr, D3DCOLOR color ) : 
	m_Size(size), m_Window(han_Window), m_pDxDevice(p_dx_device), p_dx_VertexBuffer(0), p_dx_IndexBuffer(0), flt_Angle(0), m_Colour( color ), m_TextureStr(textureStr), m_VertexFormat(D3DFVF_XYZ | D3DFVF_TEX1 )
{
	D3DXMatrixIdentity( &m_World );
	p_dx_Texture =nullptr; //unless we go ahead and init a texture, assume there isn't one
}


RenderSprite::~RenderSprite(void)
{
	if(p_dx_Texture)
	{p_dx_Texture->Release();}
	p_dx_VertexBuffer->Release();
	p_dx_IndexBuffer->Release();
}

void RenderSprite::Init()
{
	FillVertices();
	FillIndices();
	LoadTexture( m_TextureStr );

	m_VertexFormat = (D3DFVF_XYZ | D3DFVF_TEX1 );

	if (FAILED(m_pDxDevice->CreateVertexBuffer(4*sizeof(VertexPosTex), 0, m_VertexFormat , D3DPOOL_DEFAULT, &p_dx_VertexBuffer, NULL)))
	{
		MessageBox(m_Window,L"Error while creating RenderItem VertexBuffer",L"FillVertices()",MB_OK);
	}

	VOID* p_Vertices;
	if (FAILED(p_dx_VertexBuffer->Lock(0, 4*sizeof(VertexPosTex), (void**)&p_Vertices, 0)))
	{
		MessageBox(m_Window,L"Error trying to lock RenderItem VertexBuffer",L"FillVertices()",MB_OK);
	}
	else
	{
		memcpy(p_Vertices, cv_Vertices, sizeof(cv_Vertices));
		p_dx_VertexBuffer->Unlock();
	}

	if (FAILED(m_pDxDevice->CreateIndexBuffer(6*sizeof(short), D3DUSAGE_WRITEONLY,D3DFMT_INDEX16,D3DPOOL_MANAGED,&p_dx_IndexBuffer,NULL)))
	{
		MessageBox(m_Window,L"Error while creating RenderItem IndexBuffer",L"FillIndices()",MB_OK);
	}

	VOID* p_Indices;
	if (FAILED(p_dx_IndexBuffer->Lock(0, 6*sizeof(short), (void**)&p_Indices, 0)))
	{
		MessageBox(m_Window,L"Error trying to lock RenderItem IndexBuffer",L"FillIndices()",MB_OK);
	}
	else
	{
		memcpy(p_Indices, s_Indices, sizeof(s_Indices));
		p_dx_IndexBuffer->Unlock();
	}
}

void RenderSprite::LoadTexture( LPCWSTR texStr )
{
	HRESULT result = D3DXCreateTextureFromFile( m_pDxDevice, texStr, &p_dx_Texture );

	if( S_OK != result )
	{
		MessageBox(NULL,L"Texture failed to create from file!",texStr,MB_OK);
	}
}

void RenderSprite::Draw()
{
	Draw(&m_World);
}

void RenderSprite::Draw( const D3DMATRIX * pWorld )
{
	m_pDxDevice->SetTexture(0, p_dx_Texture);

	//how to draw texture
	//m_pDxDevice->SetTextureStageState(0,D3DTSS_COLOROP,D3DTOP_SELECTARG1);
	//m_pDxDevice->SetTextureStageState(0,D3DTSS_COLORARG1,D3DTA_TEXTURE);

	m_pDxDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pDxDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	m_pDxDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	m_pDxDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_pDxDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pDxDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);

	m_pDxDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	m_pDxDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	m_pDxDevice->SetTransform(D3DTS_WORLD, pWorld);
	m_pDxDevice->SetFVF( m_VertexFormat );
    m_pDxDevice->SetStreamSource(0, p_dx_VertexBuffer, 0, sizeof(VertexPosTex));
	m_pDxDevice->SetIndices(p_dx_IndexBuffer);
	m_pDxDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,0,0,4,0,2);
}

void RenderSprite::FillVertices()
{
	const float half_size = m_Size * 0.5f;
     cv_Vertices[0].x = -half_size;
     cv_Vertices[0].y = half_size;
     cv_Vertices[0].z = 0.0f;
	 cv_Vertices[0].s = 0.0f;
	 cv_Vertices[0].t = 1.0f;
 
     cv_Vertices[1].x = half_size;
     cv_Vertices[1].y = half_size;
     cv_Vertices[1].z = 0.0f;
	 cv_Vertices[1].s = 1.0f;
	 cv_Vertices[1].t = 1.0f;
 
     cv_Vertices[2].x = -half_size;
     cv_Vertices[2].y = -half_size;
     cv_Vertices[2].z = 0.0f;
	 cv_Vertices[2].s = 0.0f;
	 cv_Vertices[2].t = 0.0f;

     cv_Vertices[3].x = half_size;
     cv_Vertices[3].y = -half_size;
     cv_Vertices[3].z = 0.0f;
	 cv_Vertices[3].s = 1.0f;
	 cv_Vertices[3].t = 0.0f;
}

void RenderSprite::FillIndices()
{
     s_Indices[0]=0;
     s_Indices[1]=1;
     s_Indices[2]=2;
     s_Indices[3]=2;
     s_Indices[4]=1;
     s_Indices[5]=3; 
}
