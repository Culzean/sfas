#pragma once
#include "SearchForAStar\States\GameStateBase.h"


namespace SFAS
{

namespace States
{


class OptionsState :	public GameStateBase
{
public:
	OptionsState(void);
	~OptionsState(void);

	void Render( float dt ) {};
	bool Update( const Engine::Input *input, Engine::AudioManager *audio, float dt );

	void Setup(  );
};
}
}
