/*
	Copyright (c) 2013 D. Waine

	The maze generates a maze using Pimm's algorithm
	the details of which are returned to returned to the world for creation and rendering
	Can accept a partial piece of maze before starting
	This also defines the maze cell which contain up to 4 possible edges
	which is also used by world to place walls
*/

#pragma once

#include <vector>
#include <ctime>
#include <d3d9.h>

namespace SFAS
{

namespace Game                  
{     
	const int kMazeWall = -1;
	const int kMazeEntrance = -2;

enum MazeEdges{
	keMazeUp,
	keMazeRight,
	keMazeDown,
	keMazeLeft,

	keNumEdges
};

struct MazeCell{
	std::vector< int > m_Edges;
	
	MazeCell() : m_Visited(false),
				m_Frontier(	false )
	{
		m_Edges = std::vector<int>(keNumEdges);
		for(int i=0; i<keNumEdges ; i++)
		{
			//start with no connections
			m_Edges[i] = kMazeWall;
		}
	};
	void SetVisited( ){
		m_Visited = true;
		m_Frontier = false;
	}
	void SetFrontier()
	{
		m_Frontier = true;
	}
	bool GetVisited()
	{
		return m_Visited;
	}
	bool GetFrontier()
	{
		return m_Frontier;
	}
private:
	bool m_Visited;
	bool m_Frontier;
};

class Maze
{
public:
	Maze(void);
	Maze(int seed);
	~Maze(void);

	//for the meantime this will only produce square mazes
	std::vector<MazeCell> BuildMaze( int dimenX, HWND p_Window );
	std::vector<MazeCell> BuildMaze( std::vector<MazeCell> startCells, int dimenX, HWND p_Window );

	int GetDimX()		{		return m_MazeDimX;		};

private:

	//recursive method to add room and update vector or possible new rooms
	bool AddRoom( std::vector< int > frontierCells, std::vector<MazeCell> &mazeRooms );

	void CheckNeighbours( int newCell, std::vector<MazeCell> &mazeRooms );
	std::vector< int > FindFrontiers( std::vector<MazeCell> &mazeRooms );

	int		m_TileTotal;
	int		m_MazeDimX;
	int		m_MazeDimY;


};

}
}

