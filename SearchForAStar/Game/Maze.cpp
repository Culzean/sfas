#include "Maze.h"

using namespace::SFAS;
using namespace::Game;


Maze::Maze(void)
{
	srand((unsigned int)std::time(NULL));
}

Maze::Maze(int seed)
{
	srand(seed);
}


Maze::~Maze(void)
{
}

std::vector<MazeCell> Maze::BuildMaze( std::vector<MazeCell> startCells, int dimenX, HWND p_Window )
{
	return std::vector<MazeCell>(1);
}

std::vector<MazeCell> Maze::BuildMaze( int dimenX, HWND p_Window )
{
	//first set the requested maze dimensions
	m_MazeDimX = m_MazeDimY = dimenX;
	m_TileTotal = m_MazeDimX * m_MazeDimY;
	std::vector<MazeCell> mazeRooms = std::vector<MazeCell>(m_TileTotal);

	//place first tile as explored
	int firstTile = std::rand() % ( m_TileTotal );
	mazeRooms[firstTile].SetVisited();
	CheckNeighbours( firstTile, mazeRooms );

	if( !( AddRoom( FindFrontiers(mazeRooms), mazeRooms ) ) )
	{
		MessageBox(NULL,L"Failed to generate maze",L"BuildMaze",MB_OK);
		return std::vector<MazeCell>(1);
	}
	
	return mazeRooms;
}

bool Maze::AddRoom( std::vector< int > frontierCells, std::vector<MazeCell> &mazeRooms )
{
	//while there are still possible frontier cells to add
	//	select a new room from frontier list at random
	//	if this has any adjacent rooms that are not visited and not frontier
	//		then add to the list and continue recusion
	//otherwise return completed maze
	if( frontierCells.size() <= 0 )
	{
		return true;
	}
	int randFrontier;
	if(frontierCells.size() == 1)
	{//an edge case, no rand selection!
		randFrontier = frontierCells[0];
	}
	else
	{
		randFrontier = frontierCells [ std::rand() % ( frontierCells.size() - 1 ) ];
	}
	
	if(mazeRooms[ randFrontier ].GetVisited())
	{
		//Should not have been to this square before! something wrong with recursion
		return false;
	}
	mazeRooms[randFrontier].SetVisited();
	CheckNeighbours( randFrontier, mazeRooms );

	if( !( AddRoom( FindFrontiers(mazeRooms), mazeRooms ) ) )
	{
		return false;
	}
	//this is not the correct exit point
	return true;
}


void Maze::CheckNeighbours( int newCell, std::vector<MazeCell> &mazeRooms )
{
	//method to set correct adjacent cells
	int leftCell, rightCell, topCell, bottomCell;
	//left tile, boundary case!
	
	leftCell = newCell - 1;
	if( ( newCell ) % m_MazeDimX != 0 )
	{
		if( !mazeRooms[leftCell].GetFrontier() && !mazeRooms[leftCell].GetVisited())
		{
			mazeRooms[leftCell].SetFrontier();
			mazeRooms[leftCell].m_Edges[keMazeRight] = newCell;
			mazeRooms[newCell].m_Edges[keMazeLeft] = leftCell;
		}
	}
	//right tile, boundary again!
	rightCell = newCell +1;
	if( ( rightCell ) % m_MazeDimX != 0 )
	{
		if( !mazeRooms[rightCell].GetFrontier() && !mazeRooms[rightCell].GetVisited())
		{
			mazeRooms[rightCell].SetFrontier();
			mazeRooms[rightCell].m_Edges[keMazeLeft] = newCell;
			mazeRooms[newCell].m_Edges[keMazeRight] = rightCell;
		}
	}
	//top cell, bit different
	topCell = newCell - m_MazeDimX;
	if( topCell >= 0 )
	{
		if( !mazeRooms[topCell].GetFrontier() && !mazeRooms[topCell].GetVisited())
		{
			mazeRooms[topCell].SetFrontier();
			mazeRooms[topCell].m_Edges[keMazeDown] = newCell;
			mazeRooms[newCell].m_Edges[keMazeUp] = topCell;
		}
	}
	//bottom cell
	bottomCell = newCell + m_MazeDimX;
	if( bottomCell < m_TileTotal )
	{
		if( !mazeRooms[bottomCell].GetFrontier() && !mazeRooms[bottomCell].GetVisited() )
		{
			mazeRooms[bottomCell].SetFrontier();
			mazeRooms[bottomCell].m_Edges[keMazeUp] = newCell;
			mazeRooms[newCell].m_Edges[keMazeDown] = bottomCell;
		}
	}
}

std::vector< int > Maze::FindFrontiers( std::vector<MazeCell> &mazeRooms )
{
	std::vector< int > frontiers;

	for(int i=0; i<m_TileTotal; i++)
	{
		if( mazeRooms[i].GetFrontier() )
		{
			frontiers.push_back(i);
		}
	}
	return frontiers;
}