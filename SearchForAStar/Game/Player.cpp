//
// Source files for Search For A Star competition
// Copyright (c) 2013 L. Attwood
//
// Player gets input through Message function and updates player state based on it.
// 
// Add a summary of your changes here:
// 
// 
// 

#include "Player.h"
#include "Bullet.h"
#include "float.h"
#include "Core/Input.h"
#include "Core/XController.h"
#include "Graphics/RenderAnimated.h"
#include "Core/AudioManager.h"

using Engine::Input;
using Engine::RenderAnimated;
using SFAS::Game::Player;
using SFAS::Game::Bullet;

const float Player::sMoveForce = 8000.0f;
const float Player::sSize = 48.0f;
const float Player::sMass = 100.0f;
const float Player::sDamping = 0.39f;

Player::Player( int id, int lives ) : Entity( id, D3DXVECTOR3( 200, 200, 0 ), D3DXVECTOR3( sSize, sSize, 0.0f ), sDamping ), m_Lives( lives ), m_Score( 0 ), m_Multiplier( 1 ), m_Best( 0 )
{
	SetMass( sMass );

	for( int count = 0; count < kNumBullets; count++ )
	{
		m_Bullets[count] = new Bullet( count );
	}
}

Player::~Player(void)
{
	for( int count = 0; count < kNumBullets; count++ )
	{
		delete m_Bullets[count];
		m_Bullets[count] = 0;
	}
}

void Player::Render( Engine::RenderAnimated* drw, FLOAT dt )
{
	//update current animation
	drw->Update(dt, m_AnimationState);
	Entity::Render(drw);
}

Bullet * Player::Update( const Engine::Input * input, const Engine::AudioManager * audio, float dt )
{
	// Check for personal best
	if( m_Score > m_Best )
	{
		m_Best = m_Score;
	}

	//handle controller input
	float rightThumbX = input->GetRightThumbX();
	float rightThumbY = input->GetRightThumbY();
	float rightMag = input->GetRightMagnitude();
	float fire = input->RepeatController();

	if( abs(rightThumbX) > abs(rightThumbY) )
	{
		rightThumbY = 0.0f;
	}
	else
	{
		rightThumbX = 0.0f;
	}

	if( rightMag == 0 || !fire )
	{
		rightThumbX = 0.0f;
		rightThumbY = 0.0f;
	}
	D3DXVECTOR3 deltaForce( 0.0f, 0.0f, 0.0f );
	PollMovement( input, deltaForce );		
	AddForce(deltaForce);
	UpdateState(  );

	Bullet * bullet = 0;
	if( input->PressedWithRepeat( Input::kFireUp ) || rightThumbY > 0.0f)
	{
		bullet = Fire( 0.0f, 1.0f );
	}
	else if( input->PressedWithRepeat( Input::kFireLeft ) || rightThumbX < 0.0f )
	{
		bullet = Fire( -1.0f, 0.0f );
	}
	else if( input->PressedWithRepeat( Input::kFireRight )|| rightThumbX > 0.0f )
	{
		bullet = Fire( 1.0f, 0.0f );
	}
	else if( input->PressedWithRepeat( Input::kFireDown ) || rightThumbY < 0.0f )
	{
		bullet = Fire( 0.0f, -1.0f );
	}

	Entity::Update(dt);

	return bullet;
}

void Player::PollMovement( const Engine::Input * input, D3DXVECTOR3 &deltaForce )
{
	//the direction pressed is represented as an int
	//one is up, clockwise round
	float leftThumbX = input->GetLeftThumbX();
	float leftThumbY = input->GetLeftThumbY();
	float leftMag = input->GetLeftMagnitude();

	if( abs(leftThumbX) > abs(leftThumbY) )
	{
		leftThumbY = 0.0f;
	}
	else
	{
		leftThumbX = 0.0f;
	}

	if( leftMag == 0 )
	{
		leftThumbX = 0.0f;
		leftThumbY = 0.0f;
	}

	if( input->Held( Input::kUp ) || leftThumbY > 0 )
	{
		deltaForce.y += sMoveForce;
		m_DirFace = 1;
	}
	if( input->Held( Input::kDown ) || leftThumbY < 0 )
	{
		deltaForce.y += -sMoveForce;
		m_DirFace = 3;
	}
	if( input->Held( Input::kLeft ) || leftThumbX < 0 )
	{
		deltaForce.x = -sMoveForce;
		m_DirFace = 4;
	}
	if( input->Held( Input::kRight ) || leftThumbX > 0 )
	{
		deltaForce.x = sMoveForce;
		m_DirFace = 2;
	}
}


void Player::UpdateState(  )
{
	Entity::UpdateState();


	switch(this->m_eState)
	{
	case(Entity::ekRun):
		
		if( (Engine::ke_walk + m_DirFace) != m_AnimationState.crtCycle)
		{
			m_AnimationState.prevCycle = m_AnimationState.crtCycle;
			m_AnimationState.crtAnimTime = 10000.0f;
			m_AnimationState.crtFrame = 2000;
			m_AnimationState.crtCycle = Engine::ke_walk + m_DirFace;
		}

		break;

	case(Entity::ekIdle):

		if( (Engine::ke_idle + m_DirFace) != m_AnimationState.crtCycle)
		{
			m_AnimationState.prevCycle = m_AnimationState.crtCycle;
			m_AnimationState.crtAnimTime = 10000.0f;
			m_AnimationState.crtFrame = 2000;
			m_AnimationState.crtCycle = Engine::ke_idle + m_DirFace;
		}

		break;

	default:
		
		
		break;

	};
}

void Player::OnCollision( Entity& other )
{
	if( other.IsMoveable() && !other.IsPlayerControlled() )
	{
		// Lost a life
		m_Lives--;

		// Lose multiplier
		m_Multiplier = 1;

		// Default behavour is to die
		SetActive( false );
	}
}

void Player::OnReset()
{
}

Bullet * Player::Fire( float vx, float vy )
{
	Bullet * bullet = 0;

	for( int count = 0; count < kNumBullets; count++ )
	{
		if( !m_Bullets[count]->IsActive() )
		{
			bullet = m_Bullets[count];
			break;
		}
	}

	if( bullet != 0 )
	{
		bullet->SetPosition( GetPosition() );
		bullet->Fire( vx, vy );
	}

	return bullet;
}

