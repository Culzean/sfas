//
// Source files for Search For A Star competition
// Copyright (c) 2013 L. Attwood
//
// The World holds a player and objects and collides them against each other. 
// 
// Add a summary of your changes here:
// 
// added some static constants for placement operations
// added methods to manage the creation of maze
// method to place maze wall accoridng to maze
// fixed iterator on while loop for bullet inactive removal
// fixed double collision mix up in DoCollision 
//

#include "World.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Wall.h"
#include "Player.h"
#include "Core/Input.h"
#include "Graphics/RenderItem.h"
#include "Graphics/RenderSprite.h"
#include "Graphics/RenderAnimated.h"
#include "Core/AudioManager.h"

using SFAS::Game::World;
using SFAS::Game::Entity;
using SFAS::Game::Wall;
using SFAS::Game::Player;

const float World::sTileWidth = 96.f;
const float World::sTileHeight = 96.f;
const float World::sWallWidth = 10.0f;

World::World(LPDIRECT3DDEVICE9 p_dx_Device, HWND han_Window, int w, int h) : m_ActiveBullets(), m_Width( (float)w ), m_Height( (float)h ), m_Level( 0 ), m_NumActiveEnemies( INT_MAX ), p_Wind(han_Window)
{
	m_Entities = std::vector<Entity*>(keNumEntities);

	float halfWallWidth = sWallWidth * 0.5f;
	float halfAreaWidth = m_Width * 0.5f;
	float halfAreaHeight = m_Height * 0.5f;

	// Renderable Objects
	m_BlueSquare = new Engine::RenderAnimated( p_dx_Device, han_Window, 1, L"SearchForAStar/Res/mage_m.png" );
	m_RedSquare = new Engine::RenderItem( p_dx_Device, han_Window, 1, D3DXCOLOR( 1.0f, 0.0f, 0.0f, 1.0f ) );
	m_GreenSquare = new Engine::RenderItem( p_dx_Device, han_Window, 1, D3DXCOLOR( 0.0f, 1.0f, 0.0f, 1.0f ) );
	m_BatSprite = new Engine::RenderSprite( p_dx_Device, han_Window, 1, L"SearchForAStar/Res/bat.png" );
	m_BlueSquare->Init();
	m_RedSquare->Init();
	m_GreenSquare->Init();
	m_BatSprite->Init();

	// Player Object
	Player * player = new Player( kePlayer, kePlayerLives );
	player->SetPosition( D3DXVECTOR3( halfAreaWidth / 2.5f, halfAreaHeight / 2, 0.0f ) );
	player->SetCollidable();
	player->SetActive( true );
	m_Entities[kePlayer] = player;	

	// Create Enemies
	for( int count = 0; count < keNumEnemies; count++ )
	{
		Enemy * enemy = new Enemy( keEnemyStart + count, 
			(float)( sWallWidth + ( rand() % (int)( w - ( sWallWidth * 2 ) ) ) ), 
			(float)( sWallWidth + ( rand() % (int)( h - ( sWallWidth * 2 ) ) ) ) );
		if(count < keEnemyStart)
		{
			enemy->SetActive( true );}
		else
		{
			enemy->SetActive( false );
		}
		enemy->SetCollidable();
		m_Entities[keEnemyStart + count] = enemy;
	}

	m_NumActiveWalls = 0;

	InitMaze();
}

World::~World(void)
{
	for( int count = 0; count < keNumEntities; count++ )
	{
		delete m_Entities[count];
	}

	delete m_BlueSquare;
	delete m_RedSquare;
	delete m_GreenSquare;
	delete m_BatSprite;
}

void World::Render( float dt )
{
	// Render the player
	m_Entities[kePlayer]->Render( m_BlueSquare, dt );

	// Render the enemies
	for( int count = keEnemyStart; count < keNumEnemies + keEnemyStart; count++ )
	{
		if( m_Entities[count] != 0 )
		{
			m_Entities[count]->Render( m_BatSprite );
		}
	}
	//render the walls
	for( int count = keWallStart; count < m_NumActiveWalls + keWallStart; count++ )
	{
		if( m_Entities[count] != 0 )
		{
			m_Entities[count]->Render( m_RedSquare );
		}
	}

	// Render active bullets
	std::list<Bullet*>::const_iterator it = m_ActiveBullets.begin();
	std::list<Bullet*>::const_iterator end = m_ActiveBullets.end();
	while( it != end )
	{
		if( (*it)->IsActive() )
		{
			(*it)->Render( m_GreenSquare );
		}

		it++;
	}
}

void World::RenderDebug( Engine::TextRenderer * txt )
{
	// Render debug info
	for( int count = 0; count < keNumEntities; count++ )
	{
		m_Entities[count]->RenderDebug( txt );
	}
}

void World::Update( const Engine::Input * input, Engine::AudioManager * audio, float dt )
{
	Player * player = GetPlayerHelper();
	
	if( player->IsActive() )
	{
		// Update the player - add bullets if neccessary
		Bullet * bullet = player->Update( input, audio,dt );
		if( bullet != 0 )
		{
			m_ActiveBullets.push_back(bullet);
			//play a sound
			int rnd = rand() % 100;
			if(rnd < 40)
			{
				audio->PlayPCM("SearchForAStar/Res/Attack0.wav");
			}
			else if(rnd < 86)
			{
				audio->PlayPCM("SearchForAStar/Res/Attack1.wav");
			}
			
		}

		// Update active bullets and make a list of inactive bullets
		std::list<Bullet*> inactive;
		std::list<Bullet*>::const_iterator it = m_ActiveBullets.begin();
		std::list<Bullet*>::const_iterator end = m_ActiveBullets.end();

		while( it != end )
		{
			if( !(*it)->IsActive() )
			{
				inactive.push_back( *it );
			}
			else
			{
				(*it)->Update( dt );

				// Collide against walls
				for( int wall = 0; wall < keNumWalls; wall++ )
				{
					DoCollision( *it, m_Entities[keWallStart + wall], dt );
				}
			}

			it++;
		}
		// Remove inactive bullets
		it = inactive.begin();
		end = inactive.end();
		while( it != end )
		{
			m_ActiveBullets.remove( *it );
			it++;
		}

		// Collide player against walls
		for( int wall = 0; wall < keNumWalls; wall++ )
		{
			DoCollision( player, m_Entities[keWallStart + wall], dt );
		}

		m_NumActiveEnemies = 0;

		// Update enemies
		for( int count = kePlayer + 1; count < keWallStart; count++ )
		{
			// Check this enemy exists and is active 
			if( m_Entities[count] != 0 && !m_Entities[count]->IsActive() )
			{
				continue;
			}

			m_NumActiveEnemies++;

			// First update this enemy
			m_Entities[count]->Update( dt );

			// Collide against the player
			if( DoCollision( player, m_Entities[count], dt ) )
			{
				this->ResetLevel();
				continue;
			}

			// Collide against walls
			for( int wall = 0; wall < keNumWalls; wall++ )
			{
				DoCollision( m_Entities[count], m_Entities[keWallStart + wall], dt );
			}

			// Collide against the player bullets
			it = m_ActiveBullets.begin();
			end = m_ActiveBullets.end();
			while( it != end )
			{
				if( (*it)->IsActive() )
				{
					if( DoCollision( m_Entities[count], *it, dt ) )
					{
						GetPlayerHelper()->AddScore( keHitScore );
						audio->PlayPCM("SearchForAStar/Res/Powerup.wav");
					}
				}

				it++;
			}
		}
	}
	else
	{
		if( m_NumActiveEnemies == -1 )
		{
			ResetLevel();
			m_Entities[kePlayer]->OnReset();
		}
	}
}

void World::NewGame()
{
	m_Level = 0;
	Player * player = GetPlayerHelper();
	player->ResetScore();
	player->ResetLives( kePlayerLives );
	m_ActiveBullets.clear();
}

bool World::IsGameOver() const
{
	if( GetPlayer()->GetLivesRemaining() == 0 )
	{
		return true;
	}

	return false;
}

void World::NextLevel()
{
	// Up the level
	m_Level++;
	ResetLevel();
	m_Entities[kePlayer]->OnReset();
	m_NumActiveEnemies = INT_MAX;
	m_ActiveBullets.clear();
	//reset pos
	float halfAreaWidth = m_Width * 0.5f;
	float halfAreaHeight = m_Height * 0.5f;
	m_Entities[kePlayer]->SetPosition( D3DXVECTOR3( halfAreaWidth / 2.5f, halfAreaHeight , 0.0f ) );
	m_Entities[kePlayer]->SetVelocity( D3DXVECTOR3( 0.0f, 0.0f, 0.0f ) );
	//build new maze
	ClearWalls();
	InitMaze();
}

void World::ResetLevel()
{
	float halfAreaWidth = m_Width * 0.5f;
	float halfAreaHeight = m_Height * 0.5f;

	// Reset the player position, velocity and rotation - then set to active
	m_Entities[kePlayer]->SetPosition( D3DXVECTOR3( halfAreaWidth / 2.5f, halfAreaHeight , 0.0f ) );
	m_Entities[kePlayer]->SetVelocity( D3DXVECTOR3( 0.0f, 0.0f, 0.0f ) );
	m_Entities[kePlayer]->SetActive( true );
	m_ActiveBullets.clear();

	// Reset enemies
	for( int count = kePlayer + 1; count < keWallStart; count++ )
	{
		m_Entities[count]->SetActive( count <= m_Level );
	}
}

bool World::IsLevelFinished() const
{
	return ( m_NumActiveEnemies == 0 );
}

bool World::DoCollision( Entity * lh, Entity * rh, float dt )
{
	bool collision = false;

	if( lh != 0 && rh != 0 && lh->CheckForPossibleCollision( *rh ) )
	{
		if( lh->CheckForCollision( *rh ) )
		{
			collision = true;

			if( lh->IsCollidable() )
			{
				lh->Resolve( *rh, dt );
			}

			lh->OnCollision( *rh );
			rh->OnCollision( *lh );
		}
	}

	return collision;
}


void World::InitMaze()
{
	Maze builder;
	float indent = m_Width - (keMazeSize * sTileWidth);

	m_Maze = builder.BuildMaze( keMazeSize ,p_Wind );
	m_Maze[3 * builder.GetDimX()].m_Edges[keMazeLeft] = kMazeEntrance;

	for(unsigned int i=0; i<m_Maze.size(); i++)
	{
		for(int j=0; j<keNumEdges; j++)
		{
			if(m_Maze[i].m_Edges[j] == kMazeWall)
			{
				if(j == keMazeDown)
				{
					float xPos = indent + ( sTileWidth * 0.5f ) + ( (i % builder.GetDimX()) * sTileWidth );
					float yPos = (float)(floor( i / builder.GetDimX() ) * sTileHeight) + (sWallWidth * 0.5f) + sTileHeight;
					BuildWall( xPos, yPos, sTileWidth, true );
				}
				if(j == keMazeLeft)
				{
					float xPos = indent + ( (i % builder.GetDimX()) * sTileWidth ) + (sWallWidth * 0.5f);
					float yPos = (float)(floor( i / builder.GetDimX() ) * sTileHeight) + (sTileHeight * 0.5f);
					BuildWall( xPos, yPos, sTileWidth, false );
				}
			}
		}
		
	}
	//and then the boundary walls too!
	float halfWallWidth = sWallWidth * 0.5f;
	float halfAreaWidth = m_Width * 0.5f;
	float halfAreaHeight = m_Height * 0.5f;

	BuildWall( halfAreaWidth, halfWallWidth, m_Width, true );
	BuildWall( halfAreaWidth, m_Height - halfWallWidth, m_Width, true );
	BuildWall( halfWallWidth, halfAreaHeight, m_Width, false );
	BuildWall( m_Width - halfWallWidth, halfAreaHeight, m_Width, false );

}

bool World::BuildWall( float xPos, float yPos, float length, bool horizontal )
{
	Wall * wall = nullptr;

	if(horizontal)
	{
		wall = new Wall( keWallStart, xPos, yPos, length, sWallWidth );
	}
	else
	{
		wall = new Wall( keWallStart, xPos, yPos, sWallWidth, length );
	}

	wall->SetActive( true );
	m_Entities[keWallStart + m_NumActiveWalls++] = wall;
	return true;
}

void World::ClearWalls()
{
	for(int i=0; i<m_NumActiveWalls; i++)
	{
		if(m_Entities[i+keWallStart] != 0)
		{
			delete m_Entities[i+keWallStart];
			m_Entities[i+keWallStart] = 0;
		}
	}
	m_NumActiveWalls = 0;
}