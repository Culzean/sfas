#include "XController.h"

using Engine::XController;

const float XController::kfControllerRepeatTime = 0.1f;

XController::XController(void)
{
	m_IsController = true;
	m_ControllerNum = 0;
}


XController::~XController(void)
{
}

void XController::PrintDisconnectionError() {
	
}

bool XController::UpdateController( float dt )
{
	m_Controller.time += dt;
	if( this->GetRightMagnitude() > 0.0f && m_Controller.time >= kfControllerRepeatTime )
	{
		m_Controller.time = 0.0f;
		m_Controller.Repeat = true;
	}
	else
	{
		m_Controller.Repeat = false;
	}
	if(GetRightMagnitude() == 0.0f)
	{
		m_Controller.time = 0.0f;
		m_Controller.Repeat = true;
	}
	return true;
}

XINPUT_STATE XController::GetState() const
{
	XINPUT_STATE controllerState;
	// Zeroise the state
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));

	// Get the state
	XInputGetState(m_ControllerNum, &controllerState);

	return (controllerState);
}

bool XController::IsConnected()
{
	XINPUT_STATE controllerState;
	// Zeroise the state
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));

	// Get the state
	DWORD Result = XInputGetState(m_ControllerNum, &controllerState);

	//return the result
	return( ERROR_SUCCESS == Result );
}

void XController::Vibrate(int leftVal, int rightVal)
{
	// Create a Vibraton State
	XINPUT_VIBRATION Vibration;

	// Zeroise the Vibration
	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVal;
	Vibration.wRightMotorSpeed = rightVal;

	// Vibrate the controller
	XInputSetState(m_ControllerNum, &Vibration);
}

float XController::GetLeftMagnitude(void) const
{
	float magnitudeL = sqrt(this->GetLeftThumbX() * this->GetLeftThumbX() + this->GetLeftThumbY() * this->GetLeftThumbY());

	if (magnitudeL > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
	{
	  //clip the magnitude at its expected maximum value
	  if (magnitudeL > 32767) magnitudeL = 32767;
  
	  //adjust magnitude relative to the end of the dead zone
	  magnitudeL -= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;
	}
	else //if the controller is in the deadzone zero out the magnitude
	{
		magnitudeL = 0.0;
	}

	return magnitudeL;
}

float XController::GetRightMagnitude(void) const
{
	float magnitudeR = sqrt(this->GetRightThumbX() * this->GetRightThumbX() + this->GetRightThumbY() * this->GetRightThumbY() );

	if (magnitudeR > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
	{
	  //clip the magnitude at its expected maximum value
	  if (magnitudeR > 32767) magnitudeR = 32767;
  
	  //adjust magnitude relative to the end of the dead zone
	  magnitudeR -= XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;
	}
	else //if the controller is in the deadzone zero out the magnitude
	{
		magnitudeR = 0.0;
	}

	return magnitudeR;
}

float XController::GetLeftThumbX(void) const
{
	return this->GetState().Gamepad.sThumbLX;
}

float XController::GetLeftThumbY(void) const 
{
	return this->GetState().Gamepad.sThumbLY;
}

float XController::GetRightThumbX(void) const
{
	return this->GetState().Gamepad.sThumbRX;
}

float XController::GetRightThumbY(void) const
{
	return this->GetState().Gamepad.sThumbRY;
}

bool XController::RepeatController()	const
{ return m_Controller.Repeat; }
