//
// Source files for Search For A Star competition
// Copyright (c) 2013 D.C. Waine
//
// Res file to load and store data for wave files
// loading PCM wave
// format and size can be accessed
// simple enum to add more sound types later
// destructor is working
//
#pragma once

#define 	SAFE_DELETE_ARRAY(x)   { delete [] x; x = NULL; }
#define		SAFE_DELETE(a) if( (a) != NULL ) delete (a); (a) = NULL;

#include <Windows.h>
#include <io.h>
#include <iostream>
#include <string>

namespace Engine
{
	//might have more sound types later on
	enum SoundType{
		ke_WavPCM
	};

	struct SndData{
		enum SoundType m_SoundType;                     // is this an Ogg, WAV, etc.?
        bool m_bInitialized;                            // has the sound been initialized
        WAVEFORMATEX m_WavFormatEx;                     // description of the PCM format
        int m_LengthMilli;                              // how long the sound is in milliseconds
	};

class XWaveRes
{
public:
	XWaveRes(void);
	~XWaveRes(void);

	HRESULT OpenWave(const char* sndFileStr, HWND p_Window);

	WAVEFORMATEX GetFormat()			{ return m_Data.m_WavFormatEx; };
	DWORD GetSize()						{ return m_BufferSize; }

	BYTE*	GetStream()					{ return m_WaveStream; }

private:

	SndData			m_Data;
	BYTE*	m_WaveStream;
	DWORD	m_BufferSize;
};

}

