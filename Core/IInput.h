#pragma once

namespace Engine
{

class IInput
{
public:
	virtual ~IInput(void) {};

	virtual float GetLeftThumbX(void) const = 0;
	virtual float GetLeftThumbY(void) const = 0;
	virtual float GetLeftMagnitude(void) const = 0;
	virtual float GetRightThumbX(void) const = 0;
	virtual float GetRightThumbY(void) const = 0;
	virtual float GetRightMagnitude(void) const = 0;
	virtual bool IsConnected(void ) const= 0;
	virtual void Vibrate(int leftVal = 0, int rightVal = 0) = 0;
	virtual XINPUT_STATE GetState() const = 0;

	virtual bool IsController()	const		= 0;
	virtual bool RepeatController()	const	= 0;
	virtual bool UpdateController( float dt )	= 0;

protected:

	struct KeyState
	{
		float time;
		bool KeyDown;
		bool LastFrameKeyDown;
		bool Repeat;
	};

	KeyState	m_Controller;
	bool		m_IsController;
};
};

