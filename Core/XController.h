// Copyright (c) 2013 D. Waine
//
// XInput Controller with keyboard backup
// 
//
#pragma once
#include "d:\year3\sfas\rs2013-danielwaine\core\input.h"
#include "Core\Input.h"

#include <windows.h>
#include <XInput.h>

// NOTE: COMMENT THIS OUT IF YOU ARE NOT USING A COMPILER THAT SUPPORTS THIS METHOD OF LINKING LIBRARIES
#pragma comment(lib, "XInput.lib")

#define XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE  7849

namespace Engine
{

class XController :
	public Input
{
public:
	XController(void);
	~XController(void);

	//Getters for controller
	XINPUT_STATE GetState(void) const;
	float GetLeftThumbX(void) const;
	float GetLeftThumbY(void) const;
	float GetLeftMagnitude(void) const;
	float GetRightThumbX(void) const;
	float GetRightThumbY(void) const;
	float GetRightMagnitude(void) const;
	bool IsConnected(void);
	void PrintDisconnectionError(void);
	void Vibrate(int leftVal = 0, int rightVal = 0);

	virtual bool UpdateController( float dt );
	bool IsController()	const		{ return m_IsController; };
	bool RepeatController()	const;  // bit of a hack to get a repeat check on controller fire command

private:

	static const float kfControllerRepeatTime;

	int m_ControllerNum;

};

}

