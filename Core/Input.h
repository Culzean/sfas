//
// Source files for Search For A Star competition
// Copyright (c) 2013 L. Attwood
//
// The base application, creates the window and device.
// 
// Add a summary of your changes here:
// 
// Added boolean to check if this class or inheriting class is providing controller functionality
// added static check for controller functionality
// included XInput for state
// added extra key states for possible buttons
// 

#pragma once

#include <Windows.h>
#include <d3d9.h> 
#include <d3dx9.h>
#include <XInput.h>
#include "IInput.h"

namespace Engine
{

class Input :
	public IInput
{
public:
	Input();
	virtual ~Input(void);

	void OnKeyDown( WPARAM parameter1, LPARAM parameter2 );
	void OnKeyUp( WPARAM parameter1, LPARAM parameter2 );
	void Update( float dt );

	enum Key
	{
		kContinue,
		kUp, 
		kDown,
		kLeft,
		kRight,
		kFireUp,
		kFireDown,
		kFireLeft,
		kFireRight,
		
		kNumInputOptions
	};

	bool JustPressed( Key key ) const;
	bool JustReleased( Key key ) const;
	bool Held( Key key ) const;
	bool PressedWithRepeat( Key key ) const;

	//getters and setters to check for controller state of class and base class
	virtual XINPUT_STATE GetState() const;

	//controller interface methods
	virtual float GetLeftThumbX(void) const { return 0.0f; };
	virtual float GetLeftThumbY(void) const { return 0.0f; };
	virtual float GetLeftMagnitude(void) const { return 0.0f; };
	virtual float GetRightThumbX(void) const { return 0.0f; };
	virtual float GetRightThumbY(void) const { return 0.0f; };
	virtual float GetRightMagnitude(void) const { return 0.0f; };
	virtual bool IsConnected(void) const { return false; };
	virtual void Vibrate(int leftVal = 0, int rightVal = 0) {  };

	virtual bool IsController()	const		{ return m_IsController; };
	virtual bool RepeatController()	const		{ return false; }
 	virtual bool UpdateController( float dt )		{return false;};

protected:

	

private:

	static const int sKeyCodes[kNumInputOptions];
	static const float kfButtonRepeatTime;

	KeyState mKeyStates[kNumInputOptions];
};

static bool ControllerAvaliable()
{
	XINPUT_STATE controllerState;
	// Zeroise the state
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));

	// This is just testing the zero controller
	//No plans for a multiplayer, so not expecting to use anything in second controller spot
	DWORD Result = XInputGetState(0, &controllerState);

	//return the result
	return( ERROR_SUCCESS == Result );
}


};
