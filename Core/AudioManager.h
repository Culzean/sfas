//
// Source files for Search For A Star competition
// Copyright (c) 2013 D. Waine
//
// Sound system that implements Xaudio for simple sound effects
// This is not the most efficient with memory, small sounds only
//
// init XAudio voice
// Store loaded sound files in a map
// can request a play using string lookup to map
// free all resources when finished

#pragma once


#include <XAudio2.h>
#include <map>
#include <string>
#include <Windows.h>
#include "XWaveRes.h"
#include <d3d9.h>

namespace Engine
{
class XWaveRes;

class AudioManager
{
public:
	AudioManager(void);
	~AudioManager(void);

	bool Init( HWND p_Window );
	HRESULT LoadWave( std::string szFilename );

	HRESULT PlayPCM( std::string waveStr );

private:

	HWND				p_HWND;	//for printing error messages

	IXAudio2*					m_dx_XAudio2;
	IXAudio2MasteringVoice*		m_dx_MasteringVoice;
	IXAudio2SourceVoice*		p_dx_XSourceVoice;

	XWaveRes* GetWav( std::string waveStr );

	std::map< std::string, Engine::XWaveRes* >					m_SoundMap;
	std::map< std::string, Engine::XWaveRes* >::iterator			m_SoundItr;
};

}
