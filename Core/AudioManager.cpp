#include "AudioManager.h"

using Engine::AudioManager;
using Engine::XWaveRes;

AudioManager::AudioManager(void)
{
	p_dx_XSourceVoice = nullptr;
}


AudioManager::~AudioManager(void)
{
	//free all audio resources
	m_dx_XAudio2->StopEngine();
	p_dx_XSourceVoice->Stop();
	p_dx_XSourceVoice->DestroyVoice();
	m_dx_MasteringVoice->DestroyVoice();
	if(m_dx_XAudio2)
	{
		m_dx_XAudio2->Release();
	}
	CoUninitialize();
	

	m_SoundItr = m_SoundMap.begin();
	while(m_SoundItr != m_SoundMap.end())
	{
		SAFE_DELETE(m_SoundItr->second);
		m_SoundItr++;
	}
}

bool AudioManager::Init( HWND p_Window )
{
	//init XAudio2, with debug engine if required
	//create channel to play sound
	HRESULT hr;
	p_HWND = p_Window;

    CoInitializeEx( NULL, COINIT_MULTITHREADED );

    UINT32 flags = 0;
#ifdef _DEBUG
    flags |= XAUDIO2_DEBUG_ENGINE;
#endif

    if( FAILED( hr = XAudio2Create( &m_dx_XAudio2, flags ) ) )
    {
        MessageBox( p_Window,L"Failed to init XAudio2 engine",L"InitializeSound", MB_OK );
        CoUninitialize();
        return false;
    }

	if( FAILED( hr = m_dx_XAudio2->CreateMasteringVoice( &m_dx_MasteringVoice ) ) )
    {
		MessageBox( p_Window,L"Failed creating mastering voice",L"InitializeSound", MB_OK );
		m_dx_XAudio2->Release();
        CoUninitialize();
        return false;
    }
	m_dx_MasteringVoice->SetVolume(0.5f);

	return true;
}

HRESULT AudioManager::LoadWave( std::string szFilename )
{
	HRESULT hr;
	XWaveRes* wav = new XWaveRes();
	
	char* buffer = NULL;

	if( FAILED( hr = wav->OpenWave( szFilename.c_str(), p_HWND )  ) )
    {
		MessageBox( p_HWND,L"Failed to parse wav file",L"LoadWave", MB_OK );
		delete wav;
        return hr;
    }
	m_SoundMap.insert( std::pair< std::string, XWaveRes* >( szFilename, wav) );

	return hr;
}

HRESULT AudioManager::PlayPCM( std::string waveStr )
{
	XWaveRes* wav = GetWav(waveStr);
	HRESULT hr;

	if(NULL == wav)
	{
		MessageBox( p_HWND,L"Failed to find wav resource in memory",L"PlayPCM", MB_OK );
		hr = S_FALSE;
        return hr;
	}

	if(nullptr== p_dx_XSourceVoice){
	// Create the source voice
		if( FAILED( hr = m_dx_XAudio2->CreateSourceVoice( &p_dx_XSourceVoice, &(wav->GetFormat())) ) )
		{
			MessageBox( p_HWND,L"Failed to create a source voice",L"PlayPCM", MB_OK );
			return hr;
		}
	}

	//place into XAUDIO_BUFFER and then submit
	// Submit the wave sample data using an XAUDIO2_BUFFER structure
    XAUDIO2_BUFFER buffer = {0};
	buffer.pAudioData = wav->GetStream();
    buffer.Flags = XAUDIO2_END_OF_STREAM;  // tell the source voice not to expect any data after this buffer
	buffer.AudioBytes = wav->GetSize();

    if( FAILED( hr = p_dx_XSourceVoice->SubmitSourceBuffer( &buffer ) ) )
    {
		MessageBox( p_HWND,L"Failed to submit buffer",L"PlayPCM", MB_OK );
        p_dx_XSourceVoice->DestroyVoice();
        return hr;
    }

    hr = p_dx_XSourceVoice->Start( 0 );
	return hr;
}

XWaveRes* AudioManager::GetWav( std::string waveStr )
{
	m_SoundItr = m_SoundMap.find(waveStr);

	if( m_SoundMap.end() == m_SoundItr )
	{
		if( FAILED(LoadWave(waveStr) ) )
		{
			MessageBox( p_HWND,L"Cannot retrieve requested sound",L"GetWav()", MB_OK );
		}
	}

	return m_SoundMap[waveStr];
}