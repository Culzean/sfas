#include "XWaveRes.h"


using Engine::XWaveRes;

XWaveRes::XWaveRes(void)
{
}


XWaveRes::~XWaveRes(void)
{
	SAFE_DELETE(m_WaveStream);
}


HRESULT XWaveRes::OpenWave( const char* sndFileStr, HWND p_Window)
{
	FILE *fp = NULL;
	fp = fopen(sndFileStr, "rb");

	if(!fp)
	{
		MessageBox( p_Window,L"Failed to open .wav file",(LPCWSTR)sndFileStr, MB_OK );
		return false;
	}

	//set variables to store the sound
	char type[4];
	DWORD size, chunkSize;
	short formatType, channels;
	DWORD sampleRate, avgBytesPerSec;
	short bytesPerSample, bitsPerSample;
	DWORD dataSize;

	//read the file header
	//a sensible place to start with any f read
	//read the first bytes in the file, what should they be?
	fread (type, sizeof(char), 4, fp);
	if(type[0] != 'R' || type[1] != 'I' || type[2] != 'F' || type[3] != 'F')		//RIFF check
	{
		MessageBox( p_Window,L"No RIFF",L"OpenWave", MB_OK );
		fclose(fp);
		return false;
	}

	fread(&size, sizeof(DWORD), 1, fp );
	fread(type, sizeof(char) , 4, fp );
	if(type[0] != 'W' || type[1] != 'A' || type[2] != 'V' || type[3] != 'E')		//WAVE check
	{
		MessageBox( p_Window,L"Not Wave",L"OpenWave", MB_OK );
		fclose(fp);
		return false;
	}

	fread(type, sizeof(char),  4, fp );
	if(type[0] != 'f' || type[1] != 'm' || type[2] != 't' || type[3] != ' ')		//fmt check?
	{
		MessageBox( p_Window,L"Not fmt",L"OpenWave", MB_OK );
		fclose(fp);
		return false;
	}

	//so this is a wave file
	//how do we digest this data? don't all of these to play a sound
	fread( &chunkSize, sizeof(DWORD), 1, fp );
	fread( &formatType, sizeof(short), 1, fp );
	fread( &channels, sizeof(short), 1, fp );
	fread( &sampleRate, sizeof(DWORD), 1, fp );
	fread( &avgBytesPerSec, sizeof(DWORD), 1, fp );
	fread( &bytesPerSample, sizeof(short), 1, fp );
	fread( &bitsPerSample, sizeof(short), 1, fp );
	

	fread( type, sizeof(char), 4, fp );
	if(type[0] != 'd' || type[1] != 'a' || type[2] != 't' || type[3] != 'a')	
	{//check we are at the data now
		MessageBox( p_Window,L"No Data",L"OpenWave", MB_OK );
		fclose(fp);
		return false;
	}

	//how large is this file?
	fread( &dataSize, sizeof(DWORD), 1, fp );

	//open up a new buffer
	CHAR* charBuff = new CHAR[dataSize];
	fread(charBuff, sizeof(BYTE), dataSize, fp);

	//covert to BYTE*
	m_WaveStream = (BYTE*)charBuff;
	m_BufferSize = dataSize;

	//load relevant data in m_data
	m_Data.m_WavFormatEx.wFormatTag = WAVE_FORMAT_PCM;
	m_Data.m_WavFormatEx.nAvgBytesPerSec = (DWORD)avgBytesPerSec;
	m_Data.m_WavFormatEx.nChannels = channels;
	m_Data.m_WavFormatEx.wBitsPerSample = bitsPerSample;
	m_Data.m_WavFormatEx.nBlockAlign = (channels * bitsPerSample) / 8;
	m_Data.m_WavFormatEx.wBitsPerSample = bitsPerSample;
	m_Data.m_WavFormatEx.nSamplesPerSec = (DWORD)sampleRate;
	m_Data.m_WavFormatEx.cbSize = 0;


	m_Data.m_bInitialized = true;
	m_Data.m_SoundType = ke_WavPCM;
	//and close file
	fclose(fp);

	return true;
}